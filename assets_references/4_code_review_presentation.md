2023-08-20


After reviewing the rubrics - `MUST HAVE code review recordings (5 min segments) this can be with a peer, at a class or screencast recording` and feedback from Neil, I have made sure to add code review recordings.

These should also count towards learning outcome - `LO2.4.1.3`.

`code_review_presentation_01.mp4`:

[Click here - Code Review - Peer Review Presentation](../assets/code_review_presentation_01.mp4)

In the code review with Andrew, we went through a few significant issues. Andrew brought up the color of the sign-up button on the landing page, noting that it's currently set to blue. He said that it should instead be set to the app's secondary color. His reasoning was that if we ever need to change that color, it would automatically update everywhere in the app.

It turns out I was also coding in the AppBar (navbar) which should be turned into a widget if we're using reusable code, something I also agreed with as it would make our code more maintainable - but we should already be using the one made.

Overall, the first part of the code review I recieved constructive feedback, and I plan to work on implementing the suggested improvements.

`code_review_presentation_02.mp4`:

[Click here - Code Review - Peer Review Presentation](../assets/code_review_presentation_02.mp4)

Andrew and I discussed problems with the code, specifically how the login and sign-up pages are set up for the user.

First, Andrew highlighted a problem with the `Request` SMS Code button on the login page. He pointed out that the button on the keyboard should act as an 'Enter' sending off the request, but it doesn't actually send an SMS code, leaving the user waiting. To solve this issue, it's crucial to incorporate functional code for the button and maybe enhance the user interface to give users clearer information.

He also emphasized the need for field validation, especially when users are entering their phone number or email for login. Suggests implementing an `if` statement to switch between number and email input types, and I agree.

Next, we moved to the sign-up page. He observed that the page doesn't provide any feedback for invalid or missing entries. To illustrate, it fails to change color or show a notification when the user doesn't input an email or inputs an incorrect one. It is crucial to include validators for these input sections.It also turned out we're missing the check for minimum password length, which got me thinking about implementing some additional security features around that.

The meeting went well and the feedback was helpful, I found some ways to make the app better.

`code_review_presentation_03.mp4`:

[Click here - Code Review - Peer Review Presentation](../assets/code_review_presentation_03.mp4)

In this code review we primarily discussed various issues related to code and programming errors. Topics ranged from legacy code not serving a purpose to how to handle imports more efficiently. Specific issues discussed included:
  
1. **Legacy Code**: We noticed that some of the code was "legacy" and wasn't actually doing anything useful.

2. **Overflow Issue**: I pointed out another issue related to overflow, to which Andrew acknowledged and mentioned it's an easy fix.

3. **Print Lines**: A warning message is displayed when using print lines in production code.

4. **Imports**: Pointed out that the last issue was with relative imports, suggesting they should be package-based imports instead, keeping up with the coding convention.

`code_review_presentation_04.mp4`:

[Click here - Code Review - Peer Review Presentation](../assets/code_review_presentation_04.mp4)

In this code review we discussed:

1. Building conventions and naming conventions in the code.

2. Mentioning of the Android manifest and the app build.

3. Flutter and Gradle build.

This meeting with mostly discussed building and showing the application.

[Click here - Code Review - Peer Review Presentation - CI/CD Pipeline for Quality Assurance](../assets/code_review_presentation_pipeline_05.mp4)

In this code review presentation I went over the Github Actions pipeline with Andrew.

1. The pipeline triggers when a pull request is made, and it's based on feature branches. For example, if a landing page is being worked on, it would be in a branch like `feature-landing-page` going into `dev`

2. The pipeline runs linting for Flutter - tests to be implemented in the future.

3. The flutter version is hardcoded, we should be making sure to use the same version as out project.

4. We are using Github Secrets to handle tokens instead of hardcoding them into the pipeline, which is good practice for security.

5. Future impovements on adding the pipeline to cover `dev` -> `main` pushes.