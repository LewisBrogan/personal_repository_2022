| Summary                                               | Issue key  | Issue id | Issue Type | Status | Priority | Resolution | Assignee     | Reporter     | Creator      | Created            | Updated            | Resolved           |
|-------------------------------------------------------|------------|----------|------------|--------|----------|------------|--------------|--------------|--------------|--------------------|--------------------|--------------------|
| Investigate issue with Net Sales Cover Group failing  | CQIPS-646  | 43180    | Story      | Done   | High     | Done       | Lewis Brogan | Lewis Brogan | Lewis Brogan | 23 Sep 2022 15:14  | 26 Sep 2022 10:20  | 26 Sep 2022 10:20  |


I was working on the Net Sales Cover Group transformation in dbt and noticed that there was an issue that needed fixing. I decided to take a closer look at the code and try to understand what was going on.

To figure out what could be causing the problem, I checked if there were any recent changes or updates that could have triggered the issue. Once I had a better idea of what was happening, I started to troubleshoot the issue by testing different steps of the model and trying to isolate the problem to a specific part of the code. I also reached out to other members of the team to see if they had any ideas or could provide input on the issue. After some trial and error, I was able to figure out what was causing the issue and come up with a solution to fix it. To make sure that everyone on the team was on the same page, I documented everything I did so that others could understand what I had done and why. I also tested everything thoroughly to make sure that the issue was fully resolved. Overall, I was happy with my approach to this issue and feel like I was successful in resolving it. (LO2.4.2.3)

Here is an example of a unit testing I did for this model in SQL:

```sql
select 
  'orders total amount' as test_name 
, order_id 
, sum(quantity * price) as actual_total_amount
, expected_total_amount
from 
  order_items
join order_totals_test using (order_id)
group by 
  order_id
, expected_total_amount
having 
  sum(quantity * price) != expected_total_amount
union 
select 
  'orders total count' as test_name 
, null as order_id 
, count(*) as actual_total_count
, count(distinct order_id) as expected_total_count
from 
  order_items
having 
  count(distinct order_id) != count(*);

/*
This query first selects the order ID, actual total amount, and expected total amount for each order using a JOIN to the order_totals_test table. It then filters the results to only show rows where the actual total amount does not match the expected total amount.

The query then uses a UNION to combine the results with another query that checks the total number of orders. This second query selects a test name, NULL as the order ID, the actual total count of orders, and the expected total count of orders (which is the number of distinct order IDs in the order_items table). It then filters the results to only show rows where the actual total count does not match the expected total count.

By running this query, we can quickly test both the total amount and the total count of orders and see if they match our expected values. If any rows are returned by the query, we know that the test has failed and we need to investigate further.
```

With all of the models we create, we do unit testing. Unit testing in SQL is important for client work because it helps ensure that the code is correct and works as intended. It also makes it easier to make changes to the code in the future without introducing new bugs. Unit tests help developers work together more effectively and can serve as a type of documentation for the code. Finally, unit testing helps ensure that the final product meets the client's requirements and is of high quality. (LO2.5.2.5)

**NOTE: Due to working on client data, I was not able to take a screenshot of the relevant pages in GitHub or Jira. However, I was allowed to post the CSV from Jira with client information removed.**
