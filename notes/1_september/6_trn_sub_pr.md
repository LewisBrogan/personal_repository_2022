| Summary                              | Issue key  | Issue id | Issue Type | Status | Priority | Resolution | Assignee     | Reporter     | Creator      | Created            | Updated            | Resolved           |
|--------------------------------------|------------|----------|------------|--------|----------|------------|--------------|--------------|--------------|--------------------|--------------------|--------------------|
| Transformation for trn_subsection    | CQIPS-645  | 43174    | Story      | Done   | Medium   | Done       | Lewis Brogan | Lewis Brogan | Lewis Brogan | 22 Sep 2022 15:26  | 29 Sep 2022 09:36  | 29 Sep 2022 09:36  |

1. Analyzed the JSON data for the trn_subsection transformation.
2. Designed a dbt model to transform the JSON data into a structured format.
3. Wrote SQL code to extract the relevant data from the JSON files.
4. Tested the pipeline to ensure that it was working correctly.
5. Debugged and fixed any issues that came up during testing.

**NOTE: Due to working on client data, I was not able to take a screenshot of the relevant pages in GitHub or Jira. However, I was allowed to post the CSV from Jira with client information removed.**
