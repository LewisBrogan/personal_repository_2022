# September Notes

Here are the notes / pull requests for September:

- [Update Schema Tests for Policy Header](1_policy_header_pr.md)
- [Update transform for policy_header](2_update_policy_header_pr.md)
- [Transformation for trn_adjustments](3_trn_adj_pr.md)
- [Investigate issue with Net Sales Cover Group failing](4_cg_failing_pr.md)
- [Add new columns to stg_endorsments](5_stg_cols_pr.md)
- [Transformation for trn_subsection](6_trn_sub_pr.md)

Tools / Acronyms / Frameworks / Testing information:

- What is Data Build Tool (DBT)? https://www.getdbt.com/blog/what-exactly-is-dbt/
- What is Snowflake? https://www.snaplogic.com/blog/snowflake-data-platform
- What is Json? https://www.w3schools.com/whatis/whatis_json.asp
- What are schema tests in DBT? https://towardsdatascience.com/how-to-do-unit-testing-in-dbt-cb5fb660fbd8
- What is a data pipeline? https://www.ibm.com/uk-en/topics/data-pipeline
- What is Agile Methodology? https://www.techtarget.com/searchsoftwarequality/definition/agile-software-development

![Commits Example](commits_sep.png)

(*Due to no longer being in the company, the commits are now shown as private*)

In September, I was tasked with a variety of assignments. Here is a summary of what I worked on:

1. Updated schema tests for Policy Header in dbt involved identifying missing columns, updating the config.csv file, generating a new schema, and running tests to ensure everything was working correctly.
2. Updated the transform code for Policy Header in dbt, which involved making necessary changes to improve its functionality and performance, creating tests, and submitting a pull request for review.
3. Created a new transformation for trn_adjustments by writing SQL code to transform json data into a desired output format, writing tests, and submitting a pull request for review.
4. Investigated an issue with the Net Sales Cover Group transformation in dbt, which involved troubleshooting the code, testing different steps of the model, and isolating the problem to a specific part of the code before coming up with a solution to fix it.
5. Added new columns to the stg_endorsements staging file in dbt from json data, which involved using the snowflake syntax to flatten the json data and using dbt's json functions to lateral flatten the json data.
6. Created a transformation for trn_subsection by analysing the json data, designing a dbt model, writing SQL code to extract the relevant data from the json files, testing the pipeline, and fixing any issues during testing.

I utilized Agile methodology with weekly sprints to plan out my tasks and prioritize them based on their importance. Through daily chats with the clients about the data, I provided updates on the progress of the tasks and received feedback on any changes or adjustments needed. This approach helped me ensure that my work was aligned with the client's needs, as I was able to identify and manage project deliverables. By breaking down the information gathered into specific deliverables, I was able to better plan and prioritize my work, which allowed me to stay on track and ensure that the project met the client's needs. (LO2.5.2.1) (LO2.6.1.1)

Here are some benefits of identifying and managing project deliverables:

1.  Provides clarity and focus: By identifying and breaking down the project into specific deliverables, we can better understand the scope of the project and focus on what needs to be done. This helps us to stay on track and ensure that we are not wasting time on unnecessary tasks.

2.  Helps manage expectations: By clearly identifying the deliverables, we can manage the client's expectations and ensure that we are meeting their needs. This helps to build trust and credibility with the client, which is critical for a successful project.

3.  Enables effective communication: By breaking down the project into specific deliverables, we can communicate more effectively with the client and stakeholders. This helps to ensure that everyone is on the same page and that there are no misunderstandings about the project.

4.  Facilitates progress tracking: By identifying and managing project deliverables, we can track our progress more effectively and ensure that we are meeting our deadlines. This helps us to stay on track and identify any potential roadblocks or delays early on in the project.

So, Identifying and managing project deliverables is critical for ensuring the success of any project, especially in data development. By doing so, we can better plan and prioritize our work, manage expectations, communicate effectively, and track our progress more effectively. This helps us to deliver high-quality work that meets the client's needs and builds trust and credibility.

A few issues that came up during this working period.  

One issue I faced during September was with the transformation for the trn_subsection. The json data was more complex than anticipated, and it took some time to figure out how to extract the relevant data. To overcome this, I broke the problem down into smaller pieces and tested different approaches until I found one that worked. I also collaborated with other team members to get feedback and input, which helped me refine my technique and complete the transformation successfully.

Another issue I faced was with the stg_endorsements staging file. The data format had changed since the last update, which required me to change the existing code:

1. I reviewed the current code and identified the necessary changes to overcome this.
2. I wrote unit tests to ensure the changes worked as intended and collaborated with other team members to get feedback and input.
3. I could update the staging file through testing, collaboration, and careful code review.

So, I learned that being agile and adaptable is critical to overcoming issues and completing tasks successfully. Breaking down problems into smaller pieces, testing different approaches, and collaborating with other team members were also essential in overcoming challenges and achieving work that met the client's needs. 

The most important part I want to talk about is the unit testing this month. As someone using dbt to manage the organisation's data pipeline, I have realised the importance of SQL unit testing in dbt. Through my experience with dbt, I have learned that SQL unit testing is critical for ensuring that the data is producing accurate and trustworthy results.

By implementing unit tests in dbt, we can catch any issues or errors early on in the pipeline, which saves time and prevents us from making incorrect decisions based on inaccurate data. Additionally, unit tests allow us to have more confidence in our data pipeline, as we can be assured that any changes we make to the pipeline will not negatively impact the accuracy of the data.

Here are two examples of dbt unit tests that we have implemented in our data pipeline:
(LO2.5.2.4)

1. Column Check: This unit test checks that a specific column exists in a table and that the column's data type matches the expected data type. This test is essential for ensuring that we are collecting the correct data and that the data is being stored correctly.

```sql
{{ config(
    materialized='table'
) }}
select
    case when count(*) > 0, then 'table column exists' else 'table column does not exist end as check_result
from
    information_schema.columns
where
    table_schema = '{{ ref('my_database') }}'
    and table_name = 'my_table'
    and column_name = 'my_column'
    and data_type = 'integer';
```

2. Row Count Check: This unit test checks that the number of rows in a table matches the expected number of rows. This test is essential for ensuring that all the data is being processed correctly and that we are not losing any data during the pipeline.

```sql
{{ config(
    materialized='table'
) }}
select
    case when count(*) = {{ ref('expected_row_count') }} then 'row count matches' else 'row count does not match' end as check_result
from
    {{ ref('my_table') }};
```
Creating unit tests can often be one of the most challenging tasks when working with clients' data. This is because unit tests require a strong understanding of the data being processed and the expected outcomes of each transformation in the pipeline.

One of the biggest challenges when creating unit tests is ensuring that the tests are comprehensive and cover all possible scenarios. This can require a significant amount of time and effort and a deep understanding of the data and the pipeline.

Another challenge with unit testing in dbt is managing the complexity of the pipeline. As the pipeline grows and becomes more complex, ensuring that all tests are relevant and cover all necessary scenarios cannot be easy. This can require ongoing maintenance and updates to the tests, which can be time-consuming and require significant attention to detail.

Despite these challenges, it is essential to prioritise unit testing in dbt when working on this kind of project. Unit tests help ensure that the data pipeline produces accurate and trustworthy results, which is critical for making informed decisions based on the data. While creating comprehensive and practical unit tests can be a difficult task, it is well worth the effort to ensure the quality and reliability of the data pipeline. (LO2.5.2.5)

REFERENCES:

Brush, K. and Silverthorne, V. (2022). What is Agile Software Development (agile methodologies)? Software Quality. TechTarget. Available at: https://www.techtarget.com/searchsoftwarequality/definition/agile-software-development. 

Gao, X. (2022) How to do unit testing in DBT, Medium. Towards Data Science. Available at: https://towardsdatascience.com/how-to-do-unit-testing-in-dbt-cb5fb660fbd8. 

Handy, T., Maddali, S. and Lantz, J. (2017) What, exactly, is DBT?, Transform data in your warehouse. Available at: https://www.getdbt.com/blog/what-exactly-is-dbt/. 

Reckers, E. (2023) What is the&nbsp;snowflake&nbsp;data platform? &nbsp;, SnapLogic. Available at: https://www.snaplogic.com/blog/snowflake-data-platform. 

What is a data pipeline (no date) IBM. Available at: https://www.ibm.com/uk-en/topics/data-pipeline. 

What is JSON? (no date) What is JSON. Available at: https://www.w3schools.com/whatis/whatis_json.asp. 