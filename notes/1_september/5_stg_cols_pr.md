| Summary                               | Issue key  | Issue id | Issue Type | Status | Priority | Resolution | Assignee      | Reporter      | Creator       | Created             | Updated             | Resolved            |
|---------------------------------------|------------|----------|------------|--------|----------|------------|---------------|---------------|---------------|---------------------|---------------------|---------------------|
| Add new columns to stg_endorsments    | CQIPS-647  | 43186    | Story      | Done   | Medium   | Done       | Lewis Brogan | Lewis Brogan | Lewis Brogan | 26 Sep 2022 15:45   | 27 Sep 2022 09:27   | 27 Sep 2022 09:27   |



To add new columns to the stg_endorsements staging file in dbt from json data, I first began by using the snowflake syntax to flatten the json data. This involved using the FLATTEN function to convert a single json object into multiple columns, each column representing a key-value pair in the json object. I then used the SELECT statement to select the columns I wanted to add to the staging file.

Next, I used dbt's JSON functions to lateral flatten the JSON data. This would involve using the 'flatten' function to break down the JSON elements into individual columns and rows. For example, the query below flattens the JSON data in the column 'data' into individual columns: 

```sql
SELECT id, data:name AS name, data:age AS age 
FROM stg_endorsments
FLATTEN(data, 'data')
```

For example this is what a staging file would look like in dbt from json data:

```sql
{{ config(
    materialized="table", schema="landing"
) }}

with base as (
  select
, id
, data:name as name
, data:age as age
from stg_endorsements
flatten(data, 'data')
)

select * from base;
```



**NOTE: Due to working on client data, I was not able to take a screenshot of the relevant pages in GitHub or Jira. However, I was allowed to post the CSV from Jira with client information removed.**
