| Summary                                  | Issue key | Issue id | Issue Type | Status | Priority | Resolution | Assignee     | Reporter     | Creator      | Created            | Updated            | Resolved           |
| ---------------------------------------- | --------- | -------- | ---------- | ------ | -------- | ---------- | ------------ | ------------ | ------------ | ------------------ | ------------------ | ------------------ |
| Transformation for trn_adjustments       | CQIPS-632 | 42916    | Task       | Done   | Medium   | Done       | Lewis Brogan | Lewis Brogan | Lewis Brogan | 08 Sep 2022 11:40 | 21 Sep 2022 12:51 | 21 Sep 2022 12:51 |


I talked with the business team to get a clear understanding of what they wanted to do with the JSON data, and I wrote everything down so I wouldn't forget any important detai ls.

Then, I created a new transformation file in the DBT project and wrote some code to transform the JSON data into the desired output format for trn_adjustments. I tried to make the code easy to read by adding comments, breaking up long code blocks, and using descriptive variable names.

After that, I wrote a bunch of tests to make sure the transformation was working correctly. I wanted to cover all the edge cases and potential issues with the data, so I spent some time on this step.

Once I had the tests written, I ran them to make sure everything was working as expected. There were a few issues that came up, so I made some adjustments to the code and tests to fix those.

Finally, I was ready to create the pull request, which included the transformation file, the tests, and any other relevant documentation. I submitted it and waited for feedback from the team.

**NOTE: Due to working on client data, I was not able to take a screenshot of the relevant pages in GitHub or Jira. However, I was allowed to post the CSV from Jira with client information removed.**
