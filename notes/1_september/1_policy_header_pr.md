| Summary                                | Issue key | Issue id | Issue Type | Status | Priority | Resolution | Assignee   | Reporter     | Creator      | Created             | Updated             | Last Viewed         | Resolved            |
| -------------------------------------- | --------- | --------| ---------- | ------ | -------- | ---------- | ---------- | ------------ | ------------ | ------------------- | ------------------- | ------------------- | ------------------- |
| Update schema tests for policy_header  | CQIPS-627 | 42909   | Story      | Done   | Medium   | Done       | Lewis Brogan | Lewis Brogan | Lewis Brogan | 08 Sep 2022 10:29   | 21 Sep 2022 09:38   |    | 21 Sep 2022 09:38   |


Date: 2023-02-27

Update Schema Tests for Policy Header:

1. Reviewed the existing schema tests for Policy Header in dbt and identified that certain columns were missing.

2. Updated the `config.csv` file with the new table and column names that were added to the schema.

3. Ran the following dbt command to generate a new schema for Policy Header: `dbt run --models +policy_header --full-refresh`

4. Verified that the updated schema was generated successfully and all the expected columns were included.

5. Ran the dbt tests for Policy Header to ensure that all tests were passing: `dbt test --models +policy_header`

6. All tests passed successfully.

7. Committed the changes to the branch and created a PR.

I spent a bunch of time trying to figure out what was going on, but I just couldn't seem to get to the bottom of it. So, I decided to take a break and come back to it with fresh eyes. That actually ended up being a great idea, because when I came back to it, I realized that some of the tests weren't written correctly and were causing the false failures.

Once I fixed those tests, everything was good to go and all the tests were passing. I submitted the pull request with my updated tests and a detailed explanation of what happened.

Overall, I learned that sometimes you just gotta take a step back and approach a problem from a different angle, especially when you're feeling stuck. And, of course, it's always important to pay close attention to your tests and make sure they're actually testing what you want them to! (LO2.4.2.3)

**NOTE: Due to working on client data, I was not able to take a screenshot of the relevant pages in GitHub or Jira. However, I was allowed to post the CSV from Jira with client information removed.**




