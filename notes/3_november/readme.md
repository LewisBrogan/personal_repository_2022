# November Notes

Here are the notes / pull requests for November:

- [Update Github CI for DBT Trigger](1_update_github_ci_dbt_pr.md)
- [Add missing columns](2_add_cols_pr.md)


![November Commits](nov_commits.png)

(*Due to no longer being in the company, the commits are now shown as private*)

Now much updates, a lot of migration work analysis that can't be shown due to client work.
