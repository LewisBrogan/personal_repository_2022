# October Notes

Here are the notes / pull requests for October:

- [Create DAILY_RISK_BORDEREAUX_FEED_LEGAL_EXPENSES view](1_legal_exp_pr.md)
- [Create DAILY_RISK_BORDEREAUX_FEED_PC view](2_feed_pc_pr.md)
- [Add additional columns for cover_subsection](3_additional_cols_pr.md)
- [Move columns over to transform from staging for policy_header](4_stg_to_trn_pr.md)

Tools / Acronyms / Frameworks / Testing information:

- What is Data Build Tool (DBT)? https://www.getdbt.com/blog/what-exactly-is-dbt/
- What is Matillion? https://www.matillion.com/about/
- What is a Migration? https://www.netapp.com/data-management/what-is-data-migration/

![October Commits](oct_commits.png)

(*Due to no longer being in the company, the commits are now shown as private*)

From October onwards was focusing on a Migration from the Legacy tables in Matillion -> DBT, due to a lot of it being analysis work required for the tables being models, these are confidential.

REFERENCES:

What is data migration? – how to plan a data migration (2021) NetApp. Available at: https://www.netapp.com/data-management/what-is-data-migration/

About Us - Matillion Company information (2022) Matillion. Available at: https://www.matillion.com/about/