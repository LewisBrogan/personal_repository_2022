class IDPS:
    def __init__(self):
        self.rules = {}

    def add_rule(self, pattern, attack_type):
        self.rules[pattern] = attack_type

    def detect_attack(self, payload):
        for pattern, attack_type in self.rules.items():
            if pattern in payload:
                return True, attack_type
        return False, None

def main():
    idps = IDPS()

    # define attack patterns and their corresponding attack types
    idps.add_rule("sql injection", "sql injection")
    idps.add_rule("xss", "cross-site scripting")
    idps.add_rule("malware", "malware")
    
    while True:
        payload = input("enter network payload.. or 'exit' to quit!: ")
        if payload.lower() == "exit":
            break
        
        is_attack, attack_type = idps.detect_attack(payload)
        if is_attack:
            print(f"attack detected: {attack_type}")
            print("action taken: prevented the intrusion")
        else:
            print("no attack detected. the payload has been allowed.")

if __name__ == "__main__":
    main()
