**IDPS Simulator example**

This was adapted from https://github.com/hmzakhalid/Intrusion-Detection-Prevention-System and simplied so i could see how it works

This project involves creating a simple system that can detect and prevent intrusions. By using the `IDPS` class, you can specify attack patterns and the types of attacks they correspond to. When a network payload is provided, the script checks each rule's pattern against the payload. If a pattern is found in the payload, an attack is detected, and the script outputs the attack type and takes preventive action.

In this example, the predefined rules detect attack patterns related to `SQL injection`, `Cross-Site Scripting (XSS)`, and `Malware`.

This is an example to demonstrate `IDPS` in Python for `LO2.5.2.4`

**Requirements:**
- Python 3.x

**File Structure:**
```
idps_simulator/
│   idps.py
│   README.md
```

**Instructions:**
1. Open a terminal or command prompt and navigate to the `idps_simulator` directory - `./5_idps_example_simulator`

2. Run the script using the command: `python3 idps.py`.

Credit:

https://github.com/hmzakhalid/Intrusion-Detection-Prevention-System