# Simple Encryption and Decryption Example

This Python project demonstrates the process of encryption and decryption using the `cryptography` library. It showcases how data can be protected through encryption and then restored by decrypting it for the Learning Outcome `LO2.5.2.4`.

**File Structure:**
```
2_secure_message_transfer/
│   encryption.py
│   README.md
```

## Requirements

- Python 3.x
- `cryptography` library

You can install the `cryptography` library using the following command:

```
pip install cryptography
```

## Usage

1. Open a terminal or command prompt and navigate to `./2_secure_message_transfer`

2. Run the `encryption_decryption.py` script using the following command:

   ```
   python3 encryption_decryption.py
   ```

3. The script will generate a random encryption key, use it to encrypt a sample message, and then decrypt the encrypted message back to the original text. You will see the encrypted and decrypted messages printed in the console.

The script performs the following steps:

1. Generates a random encryption key using `Fernet.generate_key()`

2. Initializes a `Fernet` cipher suite using the generated key

3. Defines a sample message to be encrypted

4. Encrypts the message using the cipher suite and prints the encrypted message

5. Decrypts the encrypted message using the same cipher suite and prints the decrypted message