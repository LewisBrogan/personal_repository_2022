from cryptography.fernet import Fernet

# This will generate a random encryption key
key = Fernet.generate_key()
cipher_suite = Fernet(key)

# This is the message that you want decrypted
message = "This is a secret message to be decrypted"

# This will encrypt the message
encrypted_message = cipher_suite.encrypt(message.encode())
print("Encrypted message:", encrypted_message)

# This will decrypt the message and output it
decrypted_message = cipher_suite.decrypt(encrypted_message).decode()
print("Decrypted message:", decrypted_message)
