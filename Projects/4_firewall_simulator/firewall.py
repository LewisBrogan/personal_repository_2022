class Firewall:
    def __init__(self):
        self.rules = {}

    def add_rule(self, rule, action):
        self.rules[rule] = action

    def process_packet(self, source_ip):
        for rule, action in self.rules.items():
            if source_ip.startswith(rule):
                return action
        return "Allow"

def main():
    firewall = Firewall()

    # define the rules here
    firewall.add_rule("192.168.", "Block")
    firewall.add_rule("10.", "Block")
    firewall.add_rule("172.16.", "Block")
    
    while True:
        source_ip = input("enter source ip address (or 'exit' to quit): ")
        if source_ip.lower() == "exit":
            break
        
        action = firewall.process_packet(source_ip)
        print(f"action for {source_ip}: {action}")

if __name__ == "__main__":
    main()
