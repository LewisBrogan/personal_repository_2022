**Simple Firewall Simulator**

To help with getting a working firewall example in python I took inspiration from https://github.com/naklecha/firewall and created a much more simplified version.

This project simulates a basic firewall-like functionality. The Firewall class allows you to define rules and corresponding actions. When a source IP address is provided, the script checks each rule and applies the action associated with the first matching rule. If no rule matches, the default action is "Allow". This is a working example for `LO2.5.2.4`

In this example, the predefined rules block IP addresses that start with "192.168.", "10.", and "172.16.".

**Requirements:**
- Python 3.x

**File Structure:**
```
firewall_simulator/
│   firewall.py
│   README.md
```

**Instructions:**
1. Open a terminal or command prompt and navigate to the `firewall_simulator` directory `4_firewall_simulator`.


2. Run the script using the command: `python3 firewall.py`.

Credit:

https://github.com/naklecha/firewall