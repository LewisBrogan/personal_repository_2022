# Data Driven Website

This is a simple Flask application that reads data from a CSV file and displays it on a website.

The website is designed to be data driven, which means that changes to the CSV file will automatically be reflected on the website.


# Requirements

    Python 3.x
    Flask
    pandas

# Installation

    Install the required packages in your terminal or command prompt
    Run the Flask application by running python -m flask run

Usage

    Open your web browser and go to http://localhost:5000.
    You should see a table displaying the data from the CSV file
    To update the data on the website, simply modify the CSV file and refresh the page in your web browser

REFERENCES:

Grinberg, M. (no date) The flask mega-tutorial part I: Hello, world!, miguelgrinberg.com. Available at: https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world 