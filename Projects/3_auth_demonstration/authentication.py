class User:
    def __init__(self, username, password, roles):
        self.username = username
        self.password = password
        self.roles = roles


class AuthSystem:
    def __init__(self):
        self.users = []

    def register_user(self, username, password, roles):
        user = User(username, password, roles)
        self.users.append(user)
        print(f"user '{username}' has registered successfully.")

    def authenticate(self, username, password):
        for user in self.users:
            if user.username == username and user.password == password:
                return user
        return None


def main():
    auth_system = AuthSystem()
    auth_system.register_user("admin", "admin123", ["admin"])
    auth_system.register_user("user1", "user123", ["user"])

    username = input("enter username: ")
    password = input("enter password: ")
    authenticated_user = auth_system.authenticate(username, password)

    if authenticated_user:
        print(f"welcome, {authenticated_user.username}!")
        print("roles:", authenticated_user.roles)
        if "admin" in authenticated_user.roles:
            print("you have admin privileges.")
    else:
        print("Auth failed, invalid credentials")

if __name__ == "__main__":
    main()
