**Authentication and Authorization Example**

This project involves creating a simplified version of a project that imitates a fundamental system for confirming identities and granting access. It encompasses a `User` class designed to hold user details and an `AuthSystem` class intended for handling user enrollment, identity verification, and access granting, this will help as a working example for `LO2.5.2.4`

The `main` function first registers users and then asks them to provide their username and password. The script verifies whether the entered credentials belong to a registered user and allows access if the authentication is successful. If the person possesses the "admin" role, they are granted with extra privileges

**Requirements:**
- Python 3.x

**File Structure:**
```
authentication_demo/
│   authentication.py
│   README.md
```

**Instructions:**
1. Open a terminal or command prompt and navigate to the `authentication_demo` directory `./3_auth_demonstration`.

2. Run the script using the command: `python3 authentication.py`.