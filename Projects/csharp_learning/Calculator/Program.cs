﻿namespace Calculator

{
    class Program

    {
        static void Main(string[] args)
        
        {
        // https://learn.microsoft.com/en-us/visualstudio/get-started/csharp/tutorial-console?view=vs-2022 - Using this as a rough guide
            float num1 = 0; float num2 = 0;

            Console.WriteLine("Console Calculator in C#");
            for (int i = 0; i < 24; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();


            static int ReadIntFromConsole(string message)
            {
                Console.WriteLine(message);

                while (true)
                {
                    try
                    {
                        return Convert.ToInt32(Console.ReadLine());
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Invalid input. Please enter a valid number.");
                    }
                }
            }

            num1 = ReadIntFromConsole("Type a number, and then press Enter");
            num2 = ReadIntFromConsole("Type another number, and then press Enter");


            string[] options = {
                "a - Add",
                "s - Subtract",
                "m - Multiply",
                "d - Divide"
            };

            Console.WriteLine("Choose an option from the following list:");
            foreach (string option in options)
            {
                Console.WriteLine($"\t{option}");
            }

            Console.Write("Your option? ");

            switch (Console.ReadLine())
            {
                case "a":
                    Console.WriteLine($"Your result: {num1} + {num2} = " + (num1 + num2));
                    break;
                case "s":
                    Console.WriteLine($"Your result: {num1} - {num2} = " + (num1 - num2));
                    break;
                case "m":
                    Console.WriteLine($"Your result: {num1} * {num2} = " + (num1 * num2));
                    break;
                case "d":
                    Console.WriteLine($"Your result: {num1} / {num2} = " + (num1 / num2));
                    break;
            }
            
            Console.Write("Press any key to close the Calculator console app...");
            Console.ReadKey();

        }

    }

}