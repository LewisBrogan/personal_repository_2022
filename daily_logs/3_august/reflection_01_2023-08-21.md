`LO2.4.1.1: Explain and compare structure programme and project management environments, such as 'waterfall' methods, and be able to apply the underpinning philosophy and principles of agile in a project situation even in a non-agile environment. Be able to communicate technical and agile concepts to non-technical people.`

`LO2.4.2.5: Explain and apply concepts such as loose coupling, separation of concerns, gang of four, multi-tiered architectures.`

In developing the Instagram clone project with Andrew - https://github.com/AndrewEllen/instagram-clone/ (Andrew Ellen, 2023a), I made sure to apply a blend of programming and project management methodologies to create a well-structured and maintainable application. I divided the app into distinct pages like `Home`, `Landing`, `LoadingScreen`, `Login`, `NewPost`, `Profile`, each with its own Dart file. This modular design not only ensures a **separation of concerns** (`LO2.4.2.5`) but also makes the codebase easier to manage and update emphasizing the importance of creating reusable and independent modules.

While the modular approach reminds me of the `waterfall` method, where each phase is distinct and must be completed before the next begins, I also integrated agile principles into the project. For instance, I set up GitHub workflows for Continuous Integration and Continuous Deployment (CI/CD), allowing for iterative development and quick adaptations to changes. We implement agile thinking by actively putting it into practice.

To enhance collaboration on the project, we have incorporated essential resources such as a `code of conduct` - https://github.com/AndrewEllen/instagram-clone/blob/main/Team/code_of_conduct.md (Andrew Ellen, 2023b) and `coding conventions` - https://github.com/AndrewEllen/instagram-clone/blob/main/Team/coding_convensions.md (Andrew Ellen, 2023c). They not only guide the development process but also make it easier to explain technical and agile concepts to non-technical team members or stakeholders

To summarize, this project serves as an example of implementing agile principles. Additionally, it provided me with valuable expertise in effectively conveying technical and agile ideas to a wider range of people.

In my opinion, the project is a great demonstration of blending strong programming techniques with adaptable project management approaches.

REFERENCES:

Andrew Ellen (2023a). *instagram_clone*. [online] GitHub. Available from: <https://github.com/AndrewEllen/instagram-clone>

Andrew Ellen. (2023b), *instagram-clone/Team/coding_convensions.md at main · AndrewEllen/instagram-clone*. [online] Available from: <https://github.com/AndrewEllen/instagram-clone/blob/main/Team/coding_convensions.md>

Andrew Ellen. (2023c), *instagram-clone/Team/code_of_conduct.md at main · AndrewEllen/instagram-clone*. [online] Available from: <https://github.com/AndrewEllen/instagram-clone/blob/main/Team/code_of_conduct.md>.


‌