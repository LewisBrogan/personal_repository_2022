Mar 29, 2023

Guest Lecture: Colin Paice


- The role and significance of large-scale machines in handling vast numbers of transactions.

- The challenges and nuances of backend server processing, with systems handling up to 50,000 transactions per second.

- The significance of improving performance, from individual cases to various operations happening at the same time.

- The value of hardware exploitation, such as microcode instructions, to handle complex tasks efficiently.

- The emphasis on instrumentation and identifying bottlenecks to optimize performance effectively.


Webex meeting recording: Guest Lecture: Colin Paice-20230329 1202-1
Password: Wc3Eqawi
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/7100f980b057103ba677005056817305