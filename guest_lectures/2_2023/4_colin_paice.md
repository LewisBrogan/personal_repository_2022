Mar 01, 2023

Guest Lecture: Colin Paice

- The significance of 'active listening' and how it can be utilized in business environments.
- The concept of "subtext" and understanding the unsaid messages in conversations.
- Mentioned several situations which depict the difficulties and intricacies of engaging in conversations with individuals.
- The significance of documenting conversations, especially during challenging discussions.

Columns lecture was a on the importance of soft skills in the tech industry. While technical knowledge is crucial, the ability to communicate effectively and understand the nuances of conversations is equally vital. 

They highlighted common communication pitfalls that many of us fall into. The concept of "active listening" was particularly impactful. It's a skill I believe to be invaluable in both professional and personal settings. The lecture has made me more conscious of my listening habits and the importance of understanding the subtext in conversations.



Webex meeting recording: Guest Lecture: Colin Paice - The soft skills the hard way-20230301 1303-1
Password: YgPV3uWf
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/629a79929a5f103bac5c00505681049e