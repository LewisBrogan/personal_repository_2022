Feb 22, 2023

Guest Lecture: Anne Widdop

- Finding a genuine problem in the market and coming up with a successful solution is extremely important.
- The significance of conducting comprehensive market research before launching a product or service.
- Gaining insights into the intended readership and customizing solutions to meet their individual requirements.
- Selecting the appropriate business framework, such as working for oneself, forming a small business or collaborating with others.
- The difficulties in establishing a technology company, with a focus on obtaining financial support through grants and securing initial agreements.

Webex meeting recording: Guest Lecture: Anne, The VR Hive - AI and how to set up a game company-20230222 1308-1
Password: Jrp3re5q
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/f405146494df103ba75d005056816343