Mar 15, 2023

Guest Lecture: Shiela O'Hara

- The approach of making IT infrastructure green and providing solutions that promote sustainability in other organizations.
- Sheila's personal journey at IBM, from a developer to a champion of sustainable solutions.
- The "women in tech" community at IBM France and its efforts to promote women in technical roles and inspire young students.
- The importance of early exposure to science and technology for students, with programs like workplace visits for 13-year-olds.

Webex meeting recording: Guest Lecture: Sheila OHara, IBM - responsible computing-20230315 1307-1
Password: KmFmSKV3
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/3eff229ea560103ba7db005056818afd