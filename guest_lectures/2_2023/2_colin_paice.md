Feb 08, 2023

Guest Lecture: Colin Paice

He provided insights into the real-world challenges and responsibilities of working in the I.T. industry. Shared personal experiences, emphasizing the gravity of decisions made in the tech world, especially when dealing with large-scale systems and financial institutions. Highlighted the importance of following processes, the consequences of errors, and the vast scale of operations in global banks and stock exchanges.

Colin also shared a vivid description of a control room in a Chinese bank, emphasizing the magnitude of operations and the critical nature of the tasks at hand.

- Understanding the duties of the tech field and the potential results of errors is extremely important.
  
- A real-world example of a bank facing a fine of over $1 million due to a system crash caused by a configuration change.
  
- A Chinese bank oversees over 400 million active accounts, showcasing the immense scale of global organizations. Additionally, the New York Stock Exchange executes a substantial chunk of its transactions during the initial 15 minutes after the market opens.
  
- The importance of high standards and availability in the I.T. world, with companies facing severe financial consequences for downtimes.

Webex meeting recording: Guest Lecture: Colin Paice - Working in IT is Scary-20230208 1306-1
Password: iDUPD7Yj
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/502f2e9489df103b9ffd005056817e68