Jan 25, 2023

Guest Lecture: Tom McCallum

The lecture was delivered by Tom McCallum, he moved to a company called Synopsis (he was a previous lecture on our course), which specializes in simulating future hardware for various applications, such as automated driving and AI cards from NVIDIA. The company also deals with verification and security. Tom emphasized the importance of understanding the underlying mechanisms of technology, especially the binary nature of computing. 

He highlighted that everything in a computer boils down to zeros and ones, and understanding this can lead to innovation. The lecture also touched upon the structure of CPUs, the difference between floating points and integers, and the concept of Application Binary Interface (API). 

- Computing primarily relies on the binary concept, where zeros and ones are at the core of everything.

- The importance of understanding the underlying mechanisms for innovation.

- Overview of the CPU structure, highlighting the difference between floating points and integers.

- Leanne Jamieson who is a student on our course gave an Introduction to the concept of Application Binary Interface (API)


Webex meeting recording: Guest Lecture - Tom McCallum - Synopsys-20230125 1305-1
Password: BmwPaM3s
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/de7003987ede103b9dfc0050568122ee