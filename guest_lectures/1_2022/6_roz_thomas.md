Nov 24, 2022

Guest Lecture: Roz Thomas

Roslyn focused on creativity and problem-solving. Roslyn emphasized the importance of thinking outside the box and introduced the concept of divergent and convergent thinking. Divergent thinking encourages brainstorming and generating a plethora of ideas without judgment. It's about quantity and allowing for humor and playfulness. Convergent thinking, on the other hand, is about refining and selecting the best idea from the pool. It involves clustering similar ideas, seeking novelty, and being decisive. Roslyn also highlighted the barriers to creativity, such as fear of feeling foolish, our brain's default setting, the education system, and the loss of playfulness as we age. 

A practical activity was done where participants were challenged to connect nine dots using only four straight lines without lifting the pen, illustrating the need to think beyond apparent boundaries.

- **Divergent vs. Convergent Thinking:** Divergent thinking is about brainstorming and generating numerous ideas, while convergent thinking focuses on refining and selecting the best idea.
  
- **Barriers to Creativity:** These include fear of looking foolish, our brain's default setting, the standardized education system, and the loss of playfulness with age.


Webex meeting recording: Guest Lecture - Roz Thomas, Create - Creative Problem Solving-20221124 1010-1
Password: zHahr3GP
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/2d4608214e0e103bbaff00505681b03b