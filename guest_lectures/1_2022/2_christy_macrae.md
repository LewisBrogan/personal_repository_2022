Guest Lecture: Christy MacRae

Christy Macrae discusses the importance of software engineering in a modern day business. She highlights the need for cross role collaboration between technical and business teams to deliver effective solutions. She gives an example of the team structure within her organization, which includes technical leads, software engineers, testers, project managers, project owners, product owners, and technical business analysts.

She emphasizes the importance of agile development, where solutions are delivered in small chunks and tested rigorously throughout the development process. She stresses the importance of listening to customer feedback and incorporating it into the development process to improve customer experience. Also she discusses the role of APIs and their importance in the modern tech stack.

She notes that successful technology companies like Amazon, IBM, and Google are scalable, adaptable, innovative, and able to monetize their offerings. She encourages anyone interested in software engineering to familiarize themselves with APIs and their functionalities, as they are prevalent in modern tech solutions.

Summary:

- Cross role collaboration is necessary for effective software engineering solutions
- Agile development is important in delivering solutions in small chunks and testing rigorously
- Listening to customer feedback is critical to improving customer experience
- APIs are important in the modern tech stack and are prevalent in modern tech solutions
- Successful technology companies are scalable, adaptable, innovative, and able to monetize their offerings

Recording:

https://uhi.webex.com/recordingservice/sites/uhi/recording/afb0195d31d4103b8df8005056813ede/playback