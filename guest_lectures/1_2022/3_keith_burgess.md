Guest Lecture: Keith Burgess

During the presentation, Keith shared different strategies for managing and coping with the 'Imposter Phenomenon'. He acknowledged that it is a common experience and provided various techniques to overcome it. Keith highlighted that feelings are not facts, and just because you feel like an imposter, it does not mean you are one. He suggested that positive self talk and keeping a log of successes and positive reinforcement can help manage the phenomenon.

Keith also emphasized the importance of distinguishing between humility and fear, and how it is essential to avoid turning down opportunities due to self doubt. He encouraged individuals to embrace new opportunities and not let the internal imposter turn down game changing opportunities. He also emphasized the importance of talking to a mentor or manager about the syndrome to seek guidance and support.

Key note: It's hard to get rid of it completely.

Summary:

- Imposter Phenomenon is a common experience that can be managed and overcome.
- Feelings are not facts, and just because you feel like an imposter, it does not mean you are one.
- Positive self talk and keeping a log of successes and positive reinforcement can help manage the phenomenon.
- Distinguish between humility and fear and avoid turning down opportunities due to self doubt.
- Embrace new opportunities and not let the internal imposter turn down game changing opportunities.
- Talk to a mentor or manager about the Imposter Phenomenon to seek guidance and support.
- Embrace the feeling of the Imposter Phenomenon and enjoy the pain and do it anyway.