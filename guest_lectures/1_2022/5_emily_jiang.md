Nov 16th, 2022

Guest Lecture: Emily Jiang

During the guest lecture, Emily, a Java specialist at IBM, discusses the process of creating cloud-native applications using Java. Emily has significant expertise in attending conferences and collaborating on open-source projects. The lecture delves into the intricacies of developing cloud-native applications, emphasizing the importance of Java, the utilization of microservices, and the integration with cloud infrastructure.

Notes:

- **Cloud-Native Application Development:**
   - Importance of understanding new tech and best practices for microservice cloud-native apps.

- **Cloud and Cost:**
   - Cloud services come with expenses that need to be considered.
   - Importance of efficient and fast applications.

- **Java and JVM:**
   - The requirement an efficient Java Virtual Machine (JVM).
   - Comparison between Hotspot from Oracle and an open-source project from Eclipse.

- **Cloud-Native Application Characteristics:**
   - REST-based
   - scalable
   - stateless
   - configurable
   - fault-tolerant
   - discoverable
   - secure
   - monitorable
   - intelligent.

- **MicroProfile:**
   - Open standard for cloud-native application development.
   - Multiple implementations available, including Open Liberty, WildFly, and Helidon.
   - Community driven open source project.

- **Cloud Infrastructure:**
   - Emphasis on Kubernetes as a popular cloud infrastructure.
   - Importance of health checks, metrics, and open tracing for microservices.

- **Security and Configuration:**
   - Importance of securing sensitive endpoints.
   - Externalizing configuration using micro profile config.

- **Metrics and Tracing:**
   - Monitoring application performance and usage.
   - Tracing requests across multiple microservices using open tracing.

Webex meeting recording: Guest Lecture - Emily Jiang, IBM - languages/frameworks-20221116 1305-1
Password: rURgBaE9
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/4205329f47dd103bbe5700505681e0f6
