Nov 30, 2022

Guest Lecture: Jem Taylor

Jem Taylor's lecture was an interesting talk about the evolution of computing and networking. Taylor's personal experiences, both in academic and professional settings, provided a unique perspective on the challenges and advancements in the field. From the early days of computing with devices like the Honeywell 66 to the intricacies of networking protocols, The insights enlightening. The lecture not only delved into the technical aspects but also highlighted the importance of community and personal growth in the ever-evolving world of technology.

  - interactions with devices like the Honeywell 66 and the BBC Micro.
  - Discussed the foundational days of computing.
  - Discussion on the transition from X25 protocols to global Internet protocols.
  - Went on about the complexities and benefits of each protocol.
  - Emphasis on IPv4's limitations.
  - The impending shift to IPv6 to meet the digital world's demands.
  - Research focus on email instrumentation and studying academic communication patterns.
  - Insights into networking challenges and the evolution of terminal concentrators.
  - Experiences in managing extensive networks.
  - The importance of transitioning to IPv6.
  - Reflections on the swift evolution of technology.

Webex meeting recording: Guest Lecture - Jem Taylor, UHI LIS-20221130 1300-1
Password: EvVM6BGt
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/eb418ada52dc103baf9f00505681ac8b