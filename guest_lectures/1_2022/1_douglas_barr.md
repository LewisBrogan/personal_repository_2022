Oct 05, 2022

Guest Lecture: Douglas Barr

Douglas is a software engineer for Tazman-Audio, he gave us a presentation on cross platform development which involves building software that run on different operating systems (Windows, Linux, Mac). He explained that the traditional way of building software in a 'single system environment' meaning, only writing software in Windows, and not doing it in Linux as well. This way is not sufficient enough for cross platform development.

He recommended using a cross platform built tool such as CMake that can generate project files for different operating systems. He then showed us how CMake can be used with a Visual Studio project and the scripts that are used to build the project.

Also, he emphasized on the importance of version control and explained how git can be used to manage a cross platform project, and the challenges of managing dependencies across different platform and how CMake helps.

Automation and a well organized project structure was stressed as being very important to maintaining a cross platform development project, example: Jenkins.

Summary:

- The challenges of cross-platform development
- The use of cross-platform build tools such as CMake
- The importance of version control using Git
- Managing dependencies across different platforms
- The need for automation and a well-organized project structure

Recording:

https://uhi.webex.com/recordingservice/sites/uhi/recording/cc1771dc26d3103baefa00505681f0ea/playback?from_login=true