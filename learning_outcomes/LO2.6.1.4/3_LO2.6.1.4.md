`LO2.6.1.4: Explain the technical aspects of information security including client data protection and the data protection act.`

Before the data protection act and gdpr came into affect there were no 'strict' laws regulating the collection, and use of personal data. Companies were able to collect and use your personal data without consent leading to privacy concerns.

The cambridge analytica scandal in 2018 exposed how facebook user data was harvested without consent ('I made Steve Bannon's Psychological Warfare Tool': Meet the Data War Whistleblower 2018)

REFERENCES:

'I made Steve Bannon's Psychological Warfare Tool': Meet the Data War Whistleblower (2018) The Guardian. Guardian News and Media. Available at: https://www.theguardian.com/news/2018/mar/17/data-war-whistleblower-christopher-wylie-faceook-nix-bannon-trump