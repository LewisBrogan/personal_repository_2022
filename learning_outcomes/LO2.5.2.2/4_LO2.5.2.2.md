`LO2.5.2.2: Describe the industry standard software development process, including distributed work (e.g. onshore, near shore and offshore), be able to describe the benefits and drawbacks of each and key enablers to make each model successful.`

## Why do companies use different processes - Onshore, near shore and offshore?

Companies use different processes for managing distributed or onshore/offshore projects based on various factors such as project length, knowledge transfer requirements, facility use, customer team enhancement, and strategic/intangible benefits (thesai.org, n.d.). These processes are designed to address the challenges and complexities of global project management, including communication across different locations, cultural differences, and cost-effectiveness (Robb, 2021). Companies can enhance their resource allocation, boost project efficiency, and accomplish project goals by employing diverse processes (Robb, 2021).

### REFERENCES:

Robb, D. (2021). *Onshore vs. Nearshore vs. Offshore Outsourcing*. [online] CIO Insight. Available at: [https://www.cioinsight.com/news-trends/onshore-nearshore-offshore-outsourcing/](https://www.cioinsight.com/news-trends/onshore-nearshore-offshore-outsourcing/) [Accessed 27 Aug. 2023].

thesai.org. (n.d.). *Computer Science Journals | IJACSA*. [online] Available at: [https://thesai.org/Publications/IJACSA](https://thesai.org/Publications/IJACSA) [Accessed 27 Aug. 2023].