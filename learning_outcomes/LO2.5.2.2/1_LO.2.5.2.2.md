`LO2.5.2.2: Describe the industry standard software development process, including distributed work (e.g. onshore, near shore and offshore), be able to describe the benefits and drawbacks of each and key enablers to make each model successful.`

**Onshore business** activities involve hiring and operating in a company's home country. This can have a lot of benefits, including simplifying the collaboration between team members. If team members are closer geographically, they may have an easier time collaborating simply because they can meet in person. In a world where remote collaboration is ubiquitous out of necessity, it is still apparent that in-person collaboration has quite a few advantages that slim does not. (BBCIncorp - Offshore, 2023)


**Offshore** activities involve partnerships that take place in other countries. For example, if our company were to use a firm from India to handle our software development, collaboration with the Indian team would present specific challenges. The obvious challenge is operating within different time zones. Meetings may need to take place on strict schedules to ensure that everyone is available. Having an in-person meeting can be very difficult or sometimes impossible with this arrangement. (BBCIncorp - Offshore, 2023)

A crucial part of making either of these strategies work is communication and understanding other team members' schedules. During a global pandemic, even teams close together have to strike a careful balance between in-person and out of personal meetings, so the lines between onshore and offshore development are blurred.

References:

BBCIncorp - Offshore. (2023). *Onshore Vs Offshore Company: What Are The Differences?* [online] Available at: https://bbcincorp.com/offshore/articles/onshore-vs-offshore-company [Accessed 27 Aug. 2023].
‌