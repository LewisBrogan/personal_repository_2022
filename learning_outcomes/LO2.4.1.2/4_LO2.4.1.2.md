`LO2.4.1.2: Evaluate and assess in a team context basic business behaviour, ethics and courtesies, demonstrating timeliness and focus when faced with distractions and the ability to complete tasks to a deadline with high quality.`

In a team context, expected business behaviors include: 
* respect
* collaboration
* open communication
* accountability
* ethical conduct. 

Courtesies you would also expect: 
* timeliness
* adaptability
* providing constructive feedback

These business behaviour, ethics and courtesies are crucial for a positive and productive team.

The way I would demonstrate timelineness and that I'm focused and engaged on the Flutter project during the summer is by making sure I am active and engaged on the project on my off days from work - as I'm working during the summer. Consistiently completing and updating tasks on the Kanban board - which would be Trello or Clickup, keeping up communication, and doing these tasks to an agreed timeframe and meeting the deadlines set.

With regular updates on the Kanban board and constantly keeping the tasks updated my team member - Andrew can see that I'm engaged on the project and my time is well spent on tasks.

As you can see here, keeping my team mate up to date on my tasks via discord: 

![Communication](discord_comms.png)

Here on the task I've been keeping the task updated by moving it from In Progress -> Done, and also providing media evidence of the update from the task so my team member can see the update:

![Clickup Task](clickup_task_evidence.png)

So far during the project this summer, I have faced numerous distractions and there's been a couple ways I have migated this.

The main distrations I faced were external, such as noise, as I'm working nights and sleep during the day this does affect my focus. During the 4 days off from work, I have a dedicated approach to my project and university work by minimizing external distractions - earplugs, music, setting clear priorities - todo list, keeping kanban board updated, and utilizing time management techniques such as the Pomodoro Technique, breaking work into smaller tasks, setting specific goals. 

This has helped maintain my productivity and demonstrating timeliness while making the most of the limited project days available.

