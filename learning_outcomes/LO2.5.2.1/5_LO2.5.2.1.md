`LO2.5.2.1: Analyze the importance of identifying and managing project deliverables, identifying benefits for development and customer.`

During the summer myself and Andrew dove into a Flutter project collaboration for an 'Instagram clone'. With no prior knowledge of Flutter, this project was a leap into a framework I had never used before.

We encountered our first obstacle as a group when it came to decision-making. I had to grasp unfamiliar concepts - while Andy already had plenty of experience with Flutter, and we also had to discuss alternative methods. The choices we reached had a significant impact on the outcome of our project. This encounter taught me the significance of thoughtfully considering various options, weighing their pros and cons, and ultimately arriving at informed decisions.

Having empathy proved to be incredibly valuable. Even though I didn't have much expertise, we each had our own unique abilities and perspectives. Being able to acknowledge my strength and weaknesses, also Andrews, contributed to our effective collaboration. Not only did it enable us to support each other in overcoming challenges, but it also strengthened the foundation of our project.

Social perceptiveness came into play when we encountered roadblocks. Recognizing each other's frustration, confusion, or excitement allowed us to adapt our communication styles and provide encouragement. This awareness enhanced our ability to address challenges and fostered a positive atmosphere amidst the learning curve.

Project management, facilitated by Trello and ClickUp, was instrumental in our agile approach. These tools enabled us to break down tasks, set milestones, and track progress seamlessly. Regular stand-up meetings kept us aligned, ensuring that our project maintained momentum despite its complexities. E.G.:

![Backlog Creation](image.png)
![Trello in Use](image-1.png)
![Tasks in Review and Production](image-2.png)

Also a code of conduct - https://github.com/LewisBrogan/instagram-clone/blob/main/Team/code_of_conduct.md.

(LewisBrogan/Instagram-clone at dev)

Reflecting on the project, this wasn't just the replication of Instagram. It helped me with important insights into personal progress. 

Not only did I acquire a deep understanding of Flutter, but I also honed my decision-making skills, expanded my capacity for empathy, and heightened my social consciousness. These critical aptitudes, in conjunction with project management using Trello and Clickup helped create a successful project and teamwork.

Repo:  https://github.com/AndrewEllen/instagram-clone/ (Andrew Ellen, 2023)

REFERENCES:

Andrew Ellen (2023). *instagram_clone*. [online] GitHub. Available at: https://github.com/AndrewEllen/instagram-clone/ [Accessed 27 Aug. 2023].
‌
LewisBrogan, LewisBrogan/Instagram-clone at dev, GitHub. Available at: https://github.com/LewisBrogan/instagram-clone/tree/dev