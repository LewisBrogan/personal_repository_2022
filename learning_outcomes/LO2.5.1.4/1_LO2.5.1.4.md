`LO2.5.1.2: Describe the types of testing that are commonly applied to identify vulnerabilities in software and explain how to make software more resilient to threats.`


**Fuzzing** is an example of a test that might be used to identify vulnerabilities in software. The act of fuzzing involves feeding software various unexpected inputs at unexpected intervals and analyzing the software’s behavior. In many cases, normal use may never identify these types of vulnerabilities, so certain bugs can go undiscovered and be a possible attack. If these techniques are instead used in the development process, bugs that may never be discovered by other means may make it into the final product. (What is Fuzz Testing)

**Brute forcing** is another technique that can be used to test software. It is similar to fuzzing, except it more closely mimics regular user input. By putting in random, but normally formed inputs, we can find vulnerabilities that may be more akin to normal use of the software. A good example of this is for testing weak passwords. (What is a brute-force attack)

Some of the best ways to make software more secure is to think like the person attacking your software would think. By thinking like the individual doing these, we can often see our software differently and close potential threat issues before they make it into the final product. There are many resources available to help software developers think like these individuals do, and by learning about potential enemy attacks, our software can become stronger as a result.

References:
Hanna, K.T. (2021) What is a brute-force attack? - definition from TechTarget, Security. TechTarget. Available at: https://www.techtarget.com/searchsecurity/definition/brute-force-cracking
What is Fuzz Testing and how does it work? (no date) Synopsys. Available at: https://www.synopsys.com/glossary/what-is-fuzz-testing.html#:~:text=Fuzz%20testing%20or%20fuzzing%20is,as%20crashes%20or%20information%20leakage