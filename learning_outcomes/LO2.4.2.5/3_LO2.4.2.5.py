"""

Put the code seperately from learning_outcomes/LO2.4.2.5/3_LO2.4.2.5.md

"""

class Dog:
    def __init__(self, name):
        self.name = name

    def speak(self):
        return "woof"

class Cat:
    def __init__(self, name):
        self.name = name

    def speak(self):
        return "meow!"

class AnimalFactory:
    def create_animal(self, animal_type, name):
        if animal_type == "dog":
            return Dog(name)
        elif animal_type == "cat":
            return Cat(name)
        else:
            return None

factory = AnimalFactory()
dog = factory.create_animal("dog", "Donald")
cat = factory.create_animal("cat", "Duck")