"""

LO2.4.2.5: Explain and apply concepts such as loose coupling, separation of concerns,
gang of four, multi-tiered architectures.


"""

class User:
    def __init__(self, user_id, name, email):
        self.user_id = user_id
        self.name = name
        self.email = email

    def __repr__(self):
        return f"User(user_id={self.user_id}, name={self.name}, email={self.email})"


class UserRepository:
    def __init__(self):
        self.users = {
            1: User(1, "lewis", "lewis@aol.com"),
            2: User(2, "jim", "jim@aol.com"),
            3: User(3, "bob", "bob@aol.com"),
        }

    def get_user_by_id(self, user_id):
        return self.users.get(user_id, None)


class UserController:
    def __init__(self, user_repository):
        self.user_repository = user_repository

    def get_user(self, user_id):
        return self.user_repository.get_user_by_id(user_id)


user_repository = UserRepository()

user_controller = UserController(user_repository)

user = user_controller.get_user(1)
print(f"Retrieved user: {user}")

user = user_controller.get_user(4)
print(f"Retrieved user: {user}")
