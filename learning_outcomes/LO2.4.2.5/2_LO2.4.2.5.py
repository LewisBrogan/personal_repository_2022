"""

Put the code seperately from learning_outcomes/LO2.4.2.5/2_LO2.4.2.5.md

"""

class UserRepository:
    def __init__(self):
        # code to connect to database
        pass

    def get_user_by_id(self, user_id):
        # code to retrieve user from database
        pass

    def save_user(self, user):
        # code to save user to database
        pass

class UserService:
    def __init__(self, user_repository):
        self.user_repository = user_repository

    def get_user(self, user_id):
        return self.user_repository.get_user_by_id(user_id)

    def create_user(self, name, email):
        user = User(name, email)
        self.user_repository.save_user(user)
        return user

class UserController:
    def __init__(self, user_service):
        self.user_service = user_service

    def get_user(self, user_id):
        user = self.user_service.get_user(user_id)
        # code to render user as HTML
        pass

    def create_user(self, name, email):
        user = self.user_service.create_user(name, email)
        # code to redirect to user page
        pass

class User:
    def __init__(self, name, email):
        self.name = name
        self.email = email