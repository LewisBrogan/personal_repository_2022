#include <iostream>
#include <string>
#include <unordered_map>
#include <memory>

// Model Tier: Represents the data
class User {
public:
    User(int user_id, std::string name, std::string email)
        : user_id(user_id), name(name), email(email) {}

    std::string toString() const {
        return "User(user_id=" + std::to_string(user_id) +
               ", name=" + name + ", email=" + email + ")";
    }

private:
    int user_id;
    std::string name;
    std::string email;
};

// Data Access Tier: Handles the data storage
class UserRepository {
public:
    UserRepository() {
        users[1] = std::make_shared<User>(1, "lewis", "lewis@aol.com");
        users[2] = std::make_shared<User>(2, "jim", "jim@aol.com");
        users[3] = std::make_shared<User>(3, "bob", "bob@aol.com");
    }

    std::shared_ptr<User> getUserById(int user_id) {
        return users.count(user_id) ? users[user_id] : nullptr;
    }

private:
    std::unordered_map<int, std::shared_ptr<User>> users;
};

// Controller Tier: Handles the business logic
class UserController {
public:
    UserController(std::shared_ptr<UserRepository> user_repository)
        : user_repository(user_repository) {}

    std::shared_ptr<User> getUser(int user_id) {
        return user_repository->getUserById(user_id);
    }

private:
    std::shared_ptr<UserRepository> user_repository;
};

int main() {
    auto user_repository = std::make_shared<UserRepository>();

    UserController user_controller(user_repository);

    auto user = user_controller.getUser(1);
    if (user) {
        std::cout << "Retrieved user: " << user->toString() << std::endl;
    } else {
        std::cout << "Retrieved user: None" << std::endl;
    }

    user = user_controller.getUser(4);
    if (user) {
        std::cout << "Retrieved user: " << user->toString() << std::endl;
    } else {
        std::cout << "Retrieved user: None" << std::endl;
    }

    return 0;
}
