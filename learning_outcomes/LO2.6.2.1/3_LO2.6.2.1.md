`LO2.6.2.1: Explain and apply the quality assurance processes that can be automated during software development to achieve and maintain project requirements.`

During the summer, Andrew and I collaborated on an Instagram Clone project. To enhance our Quality Assurance approach, we integrated a CI/CD pipeline. A primary focus for us was refining the code through linting tests being automated before the Pull Request can be accepted.

Before diving into the pipeline integration, we first addressed a significant number of linting errors. You can delve into the details of the linting challenges we tackled in this document - [Flutter linting failures](flutter_linting_failures.md).

My initial attempt at setting up the GitHub Actions pipeline for Quality Assurance, specifically targeting linting issues, wasn't entirely successful, as evident here:

You can view the Github Actions initial workflow file here: https://github.com/AndrewEllen/instagram-clone/actions/runs/5446450526/workflow (GitHub, n.d.)

![Pull Request CICD](first_attempt_pr_cicd.gif)
![Pipeline first attempt failure](pipeline_fail.png)
![Pipeline fail Github Actions](pipeline_fail_actions.png)

Now, the issue with this failing was because of this part in the workflow code:

```yaml
jobs:
  build:
    name: Build and Test
    runs-on: ubuntu-latest

    steps:
      - name: Checkout Repository
        uses: actions/checkout@v2

      - name: Set up Flutter
        uses: subosito/flutter-action@v2
        with:
          flutter-version: '3.10.1'

      - name: Get dependencies
        run: flutter pub get

      - name: Run tests #  This here was the issue
        run: flutter test
```

Basically, we were initially wanting the Pipeline running to do the Linting, not the tests, so this was an easy fix - remove the `Run tests` step. Now the Pipeline has passed.

![Pipeline passed](pipeline_passed.png)

But, as you may have noticed in the above screenshot - `Deploy Pull Request to Dev` is greyed out, saying that it wasn't even run, as evident here:

![Skipped step](skipped_step.png)

What I was trying to do, was if the Linting passed, the pipeline would pause, wait for the Pull Request to be accepted, then it would automatically merge into `Dev` without us having to do it ourselves.

To fix this, it took several attempts. It would keep keep skipping the automatic merge into `Dev`. There's an action to have it auto merge to `Dev` once you accepted the Pull Request, you don't need to add it into github workflow actions.  You can see the amount of attempts here:

![Commit history](commit_history.png)

Eventually we just had it so we couldn't merge the Pull Request until the Linting had passed, and the Pull Request was Approved.

![Latest Pipeline](latest_pipeline.png)

Now as you can see, we have the Pipeline checking the Linting, which helps with the Quality Assurance of the project with the Pull Requests being automated inside the Pipeline.

![Pipeline latest with PR](pr_pipeline_latest.gif)

So, to summarize, Andrew and I embarked on developing an Instagram Clone. To bolster the quality of our code, we integrated a CI/CD pipeline, with a significant emphasis on automated linting tests. These tests had to be passed before any Pull Request could be approved.

Before the pipeline's full integration, we rectified numerous linting errors. A detailed account of these challenges can be found in the [Flutter linting failures](flutter_linting_failures.md) document.

Our venture into setting up the GitHub Actions pipeline, especially for linting, faced some hiccups. The initial workflow can be viewed [here](https://github.com/AndrewEllen/instagram-clone/actions/runs/5446450526/workflow) (GitHub, n.d.). The problem arose from a segment in the workflow code that mistakenly initiated tests instead of linting. After identifying and rectifying this oversight, the pipeline ran successfully.

However, another challenge emerged. A step titled `Deploy Pull Request to Dev` was consistently being skipped. Our intention was for the pipeline to pause post successful linting, await Pull Request approval, and then auto-merge into the `Dev` branch. Despite numerous attempts, as seen in the commit history, this automation remained elusive.

Ultimately, we settled on a system where a Pull Request could only be merged after successful linting and requisite approvals. This streamlined process ensured that every code change underwent linting checks, significantly enhancing the project's Quality Assurance. The automated linting within the pipeline has been instrumental in maintaining code quality and ensuring consistent adherence to coding standards.

## High-Level Overview of Quality Assurance Processes for a Non-Technical Reader

In the world of software development, think of Quality Assurance (QA) as the playtesting phase in video game creation. Just like automated bots can test various game levels for flaws, we use automated systems, specifically CI/CD pipelines, to rigorously check our code for issues. When Andrew and I worked on the Instagram Clone project, our automated "playtester" focused on "linting," essentially making sure all the game elements (code) were in the right place and interacted correctly. Although we faced some challenges in automating every step, the end result was a more reliable and high-quality project, basically the same to a well-tested, bug-free video game.

REFERENCES:

GitHub. (n.d.). *Added pipeline to project · AndrewEllen/instagram-clone@94b6757*. [online] Available at: https://github.com/AndrewEllen/instagram-clone/actions/runs/5446450526/workflow [Accessed 24 Aug. 2023].
‌