"""

In this script-the `check_https_support()` function takes a url as input and sends an http get request to the website you specified
using the requests library. It then checks the response to determine if the website supports https or not.

Basically, If the url starts with 'https' it shows that the website uses a secure https connection.

If not, it suggests that the website might be using an insecure http connection, which could pose security risks.

This is just a basic demonstration to showcase a potential client side security vulnerability for this Learning Outcome.

"""

import requests

def check_https_support(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            if response.url.startswith("https"):
                print(f"{url} supports https, meaning it'\s secure!")
            else:
                print(f"{url} does not support https. It may be vulnerable to some security risks, this should be updated")
        else:
            print(f"Failed to retrieve website. HTTP Status Code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    print("A Very basic client side security vulnerability scanner to demonstrate client side security for the learning outcome.")
    website_url = input("Enter the URL of the website you want to check: ")
    check_https_support(website_url)
