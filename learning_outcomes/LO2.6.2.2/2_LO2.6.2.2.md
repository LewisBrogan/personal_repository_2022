`LO2.6.2.2: Describe and evaluate human aspects of information security including client data protection, GDPR and the data protection act.`

**Human Aspects of Information Security:**
People are frequently regarded as the most vulnerable component in security because they can commit mistakes, be fooled, or behave harmfully, as opposed to technology that can be fortified and safeguarded. (CybSafe, 2018)

- **Awareness and Training:** Employees often lack awareness, leading to numerous security breaches. However, regular training and awareness programs can greatly assist employees in identifying and addressing security threats. (CybSafe, 2018)
  
- **Social Engineering:** This is a technique that malicious individuals employ to take advantage of human actions in order to obtain unauthorized entry to information. Phishing, baiting, and tailgating are frequently used strategies. (Learning Center, n.d.)
  
- **Insider Threats:** Employees or individuals with inside information concerning the organization's security practices, data, and computer systems can pose a threat, either maliciously or unintentionally. (Cybersecurity and Infrastructure Security Agency CISA, n.d.)

REFERENCES:

Cybersecurity and Infrastructure Security Agency CISA. (n.d.). *Defining Insider Threats | CISA*. [online] Available at: https://www.cisa.gov/topics/physical-security/insider-threat-mitigation/defining-insider-threats [Accessed 27 Aug. 2023].

CybSafe. (2018). *What actually is ‘the human aspect of cyber security’?* [online] Available at: https://www.cybsafe.com/blog/what-is-human-aspect-of-cyber-security/ [Accessed 27 Aug. 2023].

Learning Center. (n.d.).* What is Social Engineering | Attack Techniques & Prevention Methods | Imperva*. [online] Available at: https://www.imperva.com/learn/application-security/social-engineering-attack/ [Accessed 27 Aug. 2023].
‌