`LO2.4.2.1: Explain agile project delivery and show how it applies to the roll out of a software project. Explain how it ensures an accurate and timely deployment in a customer friendly way, consistent with the customer needs.`

Agile delivery is the process of releasing features incrementally. This means that certain features of the final product will be available as they are completed. This has a lot of benefits to the customer and the team that other methodologies would not be able to provide.

One benefit is the ability for the customer to start using and providing feedback on the product much earlier in the project. Not only can the customer start to gain the benefits of the product sooner, but if something needs to be changed the team can react more swiftly and make sure that future iterations do not have the same problem. This is a dual benefit for the customer and the team.

Another benefit is customer satisfaction. Showing the customer that work is being done, and allowing them to gain the benefits from that work can make the customer feel better about the entire process. Not everyone understands software engineering, so giving the customer something that they can physically interact with can help them understand how development is progressing. This benefit is more for the customer’s benefit, but it is extremely important to make the customer happy.

Reference:
Olivia (2021) An agile guide to integrating customer feedback, Focus. Available at: https://focus.meisterlabs.com/agile-guide-integrating-customer-feedback/ 