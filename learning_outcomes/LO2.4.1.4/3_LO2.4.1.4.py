"""
LO2.4.1.4

This is related to `learning_outcomes/LO2.4.1.4/3_L02.4.1.4.md`,
according to the rubric you must have scripts to run for
Communication, ICT and Numeracy skills, so i have put the
code on it's own as a working example for you to run.

"""

def addition_example(a, b):
    max_value = 1000000  # we set a maximum value to avoid integer overflows
    if a > max_value or b > max_value:
        raise ValueError('Integer overflow')
    return a + b

try:
    result = addition_example(500000, 400000)
    print(result)
except ValueError as e:
    print(e)
