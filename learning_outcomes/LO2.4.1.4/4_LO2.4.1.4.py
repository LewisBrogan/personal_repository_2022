"""
LO2.4.1.4

This is related to `learning_outcomes/LO2.4.1.4/4_LO2.4.1.4.md`,
according to the rubric you must have scripts to run for
Communication, ICT and Numeracy skills, so i have put the
code on it's own as a working example for you to run.

"""

import os
import tempfile

def safe_file_write(filename, contents):
    with tempfile.NamedTemporaryFile(delete=False) as temp_file:
        temp_file.write(contents.encode('utf-8'))
        temp_file.flush()
        os.fsync(temp_file.fileno())
        temp_path = temp_file.name

    temp_file.close()

    try:
        os.rename(temp_path, filename)
    except PermissionError as e:
        print(e)
        os.unlink(temp_path)
        raise

filename = "example.txt"
contents = "testing output"
safe_file_write(filename, contents)

with open(filename, 'r') as file:
    print(file.read())

