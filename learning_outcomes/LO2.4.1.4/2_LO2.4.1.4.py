"""
LO2.4.1.4

This is related to `learning_outcomes/LO2.4.1.4/2_L02.4.1.4.md`,
according to the rubric you must have scripts to run for
Communication, ICT and Numeracy skills, so i have put the
code on it's own as a working example for you to run.

"""

def concat_example_string(s1, s2):
    max_length = 100  # here we set the max length to avoid buffer overflows
    result = s1[:max_length] + s2[:max_length]
    return result

s1 = "hello "
s2 = "lecturers"
output = concat_example_string(s1, s2)
print(output)
