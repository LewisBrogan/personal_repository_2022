"""
LO2.4.1.4: Correctly apply the organisation’s security architecture to any particular systems or 
solutions development activities. Rewrite a simple program to remove common vulnerabilities, 
such as buffer overflows, integer overflows, and race conditions.
"""

import threading

# Function that will perform a safe addition to avoid an integer overflow
def safe_add(a, b):
    max_int = 2**31 - 1  # Here we are assuming 32-bit signed integer
    if a > max_int - b:
        return max_int
    return a + b

# Function that will perform a critical section operation but doing it in a thread safe way
def thread_safe_operation():
    global shared_variable
    lock.acquire()
    try:
        # This performs the critical section operation
        shared_variable = safe_add(shared_variable, 1)
    finally:
        lock.release()

# This initilizses the shared var and the lock
shared_variable = 0
lock = threading.Lock()

# We'll create multiple threads - 5, to simulate race conditions
threads = []
num_threads = 5

for _ in range(num_threads):
    thread = threading.Thread(target=thread_safe_operation)
    threads.append(thread)
    thread.start()

for thread in threads:
    thread.join()

print("Final value of shared variable:", shared_variable)

"""

In this rewritten program, I have defined a `safe_add` func to prevent integer overflows - which is
required for this Learning Outcome by checking if the addition of two integers exceeds the maximum valuefor
a 32-bit signed integer. We also added a lock
(threading.Lock) to the `thread_safe_operation` func to ensure that multiple threads 
can't simultaneously access and modify the `shared_variable` in a potentially unsafe manner, avoiding race conditions
which is also required in this LO.

The Lock makes sure that only one thread can access the critical section at a time.

Simply run it with `python3 ./5_LO.2.4.1.4.py`

"""