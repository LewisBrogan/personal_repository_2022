# Personal Repository for Applied Software Development Year 2022/23 #

## [Learning Outcomes](/learning_outcomes/)

## [Guest Lectures](/guest_lectures/)

## [Notes](/notes/)

## [PDP](/PDP/)

## [Projects](/Projects/)

## [Daily Logs](/daily_logs/)

## [Rubrics Evidence](/rubrics_evidence/)
* ## [Communication Tools](./rubrics_evidence/communication_tools/)
* ## [EDT Design Thinking](./rubrics_evidence/edt_design_thinking/)
* ## [Kanban](./rubrics_evidence/kanban/)
* ## [Project Management](./rubrics_evidence/project_management/)
* ## [Using the Self Assessment Tool](./rubrics_evidence/self_assessment_tool/)
* ## [Receiving Feedback](./rubrics_evidence/receiving_feedback/)