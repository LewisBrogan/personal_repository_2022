### Development Workflow Customization for Terminal

#### Step 1: Install ZSH

ZSH (Z Shell) is an extended Bourne shell with many improvements, including some features from Bash, ksh, and tcsh (Oh My Zsh, n.d.).

- **Installation Guide**: [How to Install ZSH](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH#how-to-install-zsh-on-many-platforms) (Oh My Zsh, n.d.)

##### Installation Commands for Different Platforms:

- **macOS**
  ```bash
  brew install zsh
  ```
  
- **Ubuntu**
  ```bash
  sudo apt install zsh
  ```
  
- **Windows (via WSL)**
  ```bash
  sudo apt install zsh
  ```

#### Step 2: Install Oh My Zsh

Oh My Zsh is an open-source framework for managing your Zsh configuration. It comes bundled with a ton of helpful functions, plugins, and themes (Oh My Zsh, n.d.).

- **Installation Guide**: [Oh My Zsh GitHub Repository](https://github.com/ohmyzsh/ohmyzsh) (Oh My Zsh, n.d.)

##### Installation Command:

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

#### Step 3: Install Powerlevel10k Theme

Powerlevel10k is a Zsh theme that provides an awesome interface and additional features for your terminal (Romkatv, n.d.).

- **Installation Guide**: [Powerlevel10k GitHub Repository](https://github.com/romkatv/powerlevel10k#installation) (Romkatv, n.d.)

##### Installation Command:

```bash
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

#### Step 4: Additional Goodies

1. **Zsh Autosuggestions**: This plugin suggests commands as you type, based on your history. It's particularly useful if you struggle with `cd` commands, among others (Zsh Users, n.d.a).

    - **GitHub Repository**: [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions) (Zsh Users, n.d.a)
    
    ##### Installation Command:
    ```bash
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    ```

2. **Zsh Syntax Highlighting**: This plugin provides basic syntax highlighting as you type your commands (Zsh Users, n.d.b).

    - **GitHub Repository**: [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting) (Zsh Users, n.d.b)
    
    ##### Installation Command:
    ```bash
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    ```

---

### What does it looks like?

![Theme](../rubrics_evidence/development_workspace_customisation/power10ktheme_workspace_customisation.png)

![Terminal](../rubrics_evidence/development_workspace_customisation/terminal_workspace_customisation.png)

![My Terminal](../rubrics_evidence/development_workspace_customisation/development_workspace_customisation_evidence.png)

---

### References

- Oh My Zsh. (n.d.). Installing ZSH. Available at: [GitHub Repository](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH#how-to-install-zsh-on-many-platforms)
  
- Oh My Zsh. (n.d.). Oh My Zsh GitHub Repository. Available at: [GitHub Repository](https://github.com/ohmyzsh/ohmyzsh)

- Romkatv. (n.d.). Powerlevel10k GitHub Repository. Available at: [GitHub Repository](https://github.com/romkatv/powerlevel10k#installation)

- Zsh Users. (n.d.a). Zsh Autosuggestions GitHub Repository. Available at: [GitHub Repository](https://github.com/zsh-users/zsh-autosuggestions)

- Zsh Users. (n.d.b). Zsh Syntax Highlighting GitHub Repository. Available at: [GitHub Repository](https://github.com/zsh-users/zsh-syntax-highlighting)