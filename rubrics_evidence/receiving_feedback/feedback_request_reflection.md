22 Aug, 2023.

# Requested feedback for `Feedback request LO2.5.1.5` (uhi.atlassian.net, 2023).

Thanks to the feedback, I improved the references and citations on my submission, as you can see [here](https://gitlab.com/LewisBrogan/personal_repository_2022/-/commit/5d5687b7174fba97ef2ac04cac779922f25edbb1?view=parallel):

![Updated](updated_references_and_citations_feedback.png)

(Brogan, 2023)

---

### Jira & Feedback:

![Jira Evidence for Feedback](jira_evidence_feedback.png)

![Jira Evidence for Feedback](jira_evidence_feedback_1.png)

![Jira Evidence for Feedback](jira_evidence_feedback_2.png)

---

### References:

Brogan, L. (2023). *updated references and citations (5d5687b7) · Commits · Lewis Brogan / personal_repository_2022 · GitLab*. [online] GitLab. Available at: https://gitlab.com/LewisBrogan/personal_repository_2022/-/commit/5d5687b7174fba97ef2ac04cac779922f25edbb1?view=parallel [Accessed 27 Aug. 2023].

uhi.atlassian.net. (2023). *Agile Board - Jira*. [online] Available at: https://uhi.atlassian.net/jira/software/projects/SFR/boards/116?selectedIssue=SFR-97 [Accessed 27 Aug. 2023].
‌