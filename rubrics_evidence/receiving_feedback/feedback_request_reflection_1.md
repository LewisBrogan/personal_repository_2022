24 Aug, 2023.

# Requested feedback for `Feedback req: LO2.4.2.5/LO2.4.1.1` (uhi.atlassian.net, 2023).

I was pretty happy to hear that my citations and references were getting better, so with the feedback I recieved I updated the submission [here](https://gitlab.com/LewisBrogan/personal_repository_2022/-/commits/master/daily_logs/3_august/reflection_01_2023-08-21.md) (Brogan, 2023):

![Gitlab Commit](gitlab_updated_references_citations.png)

![Gitlab Commit](gitlab_updated_references_citations_1.png)

---

### Jira & Feedback:

![Jira Feedback](jira_evidence_feedback_3.png)

![Jira Feedback](jira_evidence_feedback_4.png)

![Jira Feedback](jira_evidence_feedback_5.png)

---

### References:

Brogan, L. (2023). *Commits · master · Lewis Brogan / personal_repository_2022 · GitLab*. [online] GitLab. Available at: https://gitlab.com/LewisBrogan/personal_repository_2022/-/commits/master/daily_logs/3_august/reflection_01_2023-08-21.md [Accessed 27 Aug. 2023].

uhi.atlassian.net. (2023). *Agile Board - Jira*. [online] Available at: https://uhi.atlassian.net/jira/software/projects/SFR/boards/116?selectedIssue=SFR-98 [Accessed 27 Aug. 2023].
‌