22 Aug, 2023.

# Requested feedback for `Feedback req: LO2.4.2.2` (uhi.atlassian.net, 2023).

![Updated reference](gitlab_updated_references_reference.png)

---

### Jira & Feedback:

![Jira Evidence](jira_evidence_feedback_6.png)

![Jira Evidence](jira_evidence_feedback_9.png)

![Jira Evidence](jira_evidence_feedback_10.png)

---

### References:

uhi.atlassian.net. (2023). *Agile Board - Jira*. [online] Available at: https://uhi.atlassian.net/jira/software/projects/SFR/boards/116?selectedIssue=SFR-99 [Accessed 27 Aug. 2023].
‌